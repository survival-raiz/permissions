package com.gitlab.survivalRaiz.permissions.listeners;

import com.gitlab.survivalRaiz.permissions.Permissions;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class LoginListener implements Listener {

    private final Permissions plugin;

    public LoginListener(Permissions plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        plugin.getPermissionManager().setupPermissions(e.getPlayer());

        e.getPlayer().setDisplayName(
                ChatColor.translateAlternateColorCodes('&',
                        plugin.getCfg().getTitle(e.getPlayer().getUniqueId()) + "&r " + e.getPlayer().getName()));
    }
}
