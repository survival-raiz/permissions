package com.gitlab.survivalRaiz.permissions.config;

import api.skwead.storage.file.yml.YMLConfig;
import com.gitlab.survivalRaiz.permissions.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Handles all users' and groups' permissions
 * <p>
 * The file is organized in the following manner:
 * rankA:
 * default: \<true/false\>   # "false" by default. If "true" then the "members" parameter can be omitted
 * tag: \<string\>           # the string to appear before the user names of group members.
 * permissions: [...]      # a list of all permissions the group has, start the string with a '-' and the permission will be removed
 * members: [...]          # a list of all members' UUIDs
 * <p>
 * rankB:                   # the config is a cascade, all players in a rank will have all ranks above them.
 * \<...\>
 */
public class UsersConfig extends YMLConfig {

    private final Permissions plugin;

    public UsersConfig(Permissions plugin) {
        super("ranks", plugin);
        this.plugin = plugin;
    }

    @Override
    public void createConfig(Set<String> set) {
    }

    public List<String> getAllPerms(UUID p) {
        final List<String> perms = new ArrayList<>();
        final List<String> perms4all = new ArrayList<>();

        for (String rank : Objects.requireNonNull(getConfig().getConfigurationSection("")).getKeys(false)) {
            final List<String> rankPerms = getConfig().getStringList(rank + ".permissions");

            System.out.println(rank + " is default? " +getConfig().getBoolean(rank + ".default"));

            if (getConfig().getBoolean(rank + ".default")) {
                System.out.println("adding...");
                perms4all.addAll(rankPerms);
            } else {
                perms.addAll(rankPerms);

                if (getConfig().getStringList(rank + ".members").contains(p.toString())) {
                    perms.addAll(perms4all);

                    System.out.println("\nperms4all:");
                    perms4all.forEach(System.out::println);

                    System.out.println("\nperms:");
                    perms.forEach(System.out::println);

                    return perms;
                }
            }
        }

        return perms4all;
    }

    /**
     * Fetches the top level rank of the player and returns it's tag.
     * If none is found, "" is returned
     *
     * @param player the UUID of the player
     * @return The player's highest rank's tag, or "" if none
     */
    public String getTitle(UUID player) {
        String highestRank = "";

        for (String rank : Objects.requireNonNull(getConfig().getConfigurationSection("")).getKeys(false)) {
            final List<String> members = getConfig().getStringList(rank + ".members");
            final String uuid = player.toString();

            if (members.contains(uuid) || getConfig().getString(rank + ".default").equals("true")) {
                highestRank = getConfig().getString(rank+".tag");
            }
        }

        return highestRank;
    }

    /**
     * Adds the user's UUID to it's the new rank, removing from the previous.
     * If the rank is not found the new rank will be the default one(s).
     * The permissions are set as soon as the config is updated, but a new config read is required.
     * Although slow, this method is not that bad because it saves some memory by not storing the new permissions while it is running, and the action is not required to be that fast.
     *
     * @param player  the palyer to be updated
     * @param newRank the player's new rank
     */
    public void updatePlayer(UUID player, String newRank) {
        for (String rank : Objects.requireNonNull(getConfig().getConfigurationSection("")).getKeys(false)) {
            final List<String> members = getConfig().getStringList(rank + ".members");
            final String uuid = player.toString();

            if (members.contains(uuid)) {
                members.remove(uuid);
            } else {
                try {
                    if (rank.equals(newRank) || getConfig().getString(rank + ".tag").equals(newRank)) {
                        members.add(uuid);
                        continue;
                    }
                } catch (NullPointerException exception) {
                    continue;
                }
                continue;
            }

            getConfig().set(rank + ".members", members);
        }

        final Player p = Bukkit.getPlayer(player);
        if ( p != null)
            plugin.getPermissionManager().setupPermissions(p);
    }
}
