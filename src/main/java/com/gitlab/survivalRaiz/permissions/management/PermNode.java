package com.gitlab.survivalRaiz.permissions.management;

import com.gitlab.survivalRaiz.permissions.Permissions;
import org.bukkit.entity.Player;

public class PermNode {
    private final String value;
    private final PermNode parent;

    PermNode(String value, PermNode parent) {
        this.value = value;
        this.parent = parent;
    }

    PermNode(String value) {
        this.value = value;
        this.parent = null;
    }

    /**
     * Will check if the player has this permission
     *
     * @param player The player to be checked
     * @return true of the player has this permission or any of it's parents
     */
    public boolean allows(Player player) {
        if (player.hasPermission(getValue()))
            return true;

        if (parent == null)
            return false;

        return parent.allows(player);
    }

    PermNode getParent() {
        return parent;
    }

    public String getValue() {
        if (parent == null)
            return value;

        return parent.getValue() + "." + value;
    }

    /**
     * Updates this permission for the player
     *
     * @param p      the target player
     * @param value  the new value for this permission
     * @param plugin the permissions plugin
     */
    public void update(Player p, boolean value, Permissions plugin) {
        plugin.getPermissionManager().setupPermission(getValue(), p, value);

        plugin.getCfg().updatePlayer(p.getUniqueId(), getValue());
    }
}
