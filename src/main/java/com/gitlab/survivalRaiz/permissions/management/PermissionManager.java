package com.gitlab.survivalRaiz.permissions.management;

import com.gitlab.survivalRaiz.permissions.Permissions;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Set;

// https://github.com/The-SourceCode/Advanced_Bukkit_Coding/blob/master/Episode_13/PermissionsPlugin/src/me/tsccoding/tutorial/TutorialMain.java

/**
 * Defines a set of actions that ease player permission handling
 */
public class PermissionManager {

    private final Permissions plugin;

    public PermissionManager(Permissions plugin) {
        this.plugin = plugin;
    }

    /**
     * Gives/takes all permissions for this player
     * @param p the player whose permissions changed
     */
    public void setupPermissions(Player p) {
        final List<String> perms = plugin.getCfg().getAllPerms(p.getUniqueId());

        perms.forEach(perm -> setupPermission(perm, p, !perm.startsWith("-")));

        System.out.println("\nRESULT:");
        perms.forEach(System.out::println);
    }

    /**
     * Gives or takes a permission for a certain player.
     *
     * @param perm   The permission to be set
     * @param player The player to be modified
     * @param value  true if the permission should be given, false if it should be removed
     */
    void setupPermission(String perm, Player player, boolean value) {
        player.addAttachment(plugin).setPermission(perm, value);
    }
}
