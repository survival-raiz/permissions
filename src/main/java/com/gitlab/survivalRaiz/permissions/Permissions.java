package com.gitlab.survivalRaiz.permissions;

import com.gitlab.survivalRaiz.permissions.config.UsersConfig;
import com.gitlab.survivalRaiz.permissions.listeners.LoginListener;
import com.gitlab.survivalRaiz.permissions.management.PermissionManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Permissions extends JavaPlugin {

    private UsersConfig cfg;
    private PermissionManager permissionManager;

    @Override
    public void onEnable() {
        cfg = new UsersConfig(this);
        permissionManager = new PermissionManager(this);

        new LoginListener(this);
    }

    public PermissionManager getPermissionManager() {
        return permissionManager;
    }

    public UsersConfig getCfg() {
        return cfg;
    }
}
